<style>
  body{
    margin:0;
  }
  .start-exploring-bg{
    background-color: #9d002c;
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }

  .se-row{
    flex-direction: row;
    display: flex;
    justify-content: center;
    align-items: center;
    padding:30px;
  }
  .se-row img{
    max-width:300px;
    height:auto;
  }

  #cloud-left{
    position: relative;
    width: 120px;
    bottom: 25px;
    left: 42px;
  }

  #cloud-right{
    width: 100px;
    position: relative;
    top: 25px;
    right: 37px;
  }
</style>

<div class="start-exploring-bg">
  <div class="se-row">
    <img src="<?php echo get_stylesheet_directory_uri() . '/featured-trails/assets/ohio-trails.png' ?>" />
  </div>
  <div class="se-row">
    <img src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/dotted-trail-left.png' ?>" />
    <img id="cloud-left" src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/cloud-3.png' ?>" />
    <a id="show-map" href="#" title="Start Exploring">
      <img src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/start-exploring-map.png' ?>" />
    </a>
    <img id="cloud-right" src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/cloud-1.png' ?>" />
    <img src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/dotted-trail-right.png' ?>" />
  </div>
</div>