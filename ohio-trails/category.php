<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.0
 * 
 * Template Name: Category Page
 * Template Post Type: trail-head
 */

get_header(); 

$category = get_query_var('cat');
$current_cat = get_category($cat);
$category_id = $current_cat->term_id;
$cat_parent = get_term( $current_cat->category_parent, 'category' ); 

if($cat_parent->slug == 'lake-erie-birding-trail' ) {
	$findStops = array(
		'post_type' => 'trail-stops',
		'category_name' => $current_cat->slug,
		'posts_per_page' => -1
	);
	$trails = query_posts($findStops);
} else {
	$findHead = array(
		'post_type' => 'trail-head',
		'name' => $current_cat->slug,
		'posts_per_page' => -1
	);
	$trails = query_posts($findHead);

	$findStops = array(
		'post_type' => 'trail-stops',
		'category_name' => $current_cat->slug,
		'posts_per_page' => -1
	);
	$trailStops = query_posts($findStops);
}


?>

<div id="trail-home">
	<a href="/map"><i class="fas fa-angle-left"></i> Trails</a> / <span><?php echo $current_cat->name; ?></span>
</div>

<?php foreach ($trails as $post) :  setup_postdata($post); ?>
<div id="application" data-catid="<?php echo $category_id; ?>" data-post-tag="<?php echo $current_cat->slug; ?>" data-birding="<?php if($current_cat->slug == 'lake-erie-birding-trail'){ echo 'birding'; } ?>">
	<div id="mapCanvas"></div>
	<div id="infoWindow" data-page-thumb="<?php echo get_the_post_thumbnail_url($post->ID); ?>">
		<button id="iwClose" class="closePanel" type="button"><span>close</span><i class="fas fa-times"></i></button>
		<div class="carousel-wrap">
			<div id="carousel"></div>
		</div>
		<div class="btn-wrap">
			<button id="prev-trail" type='button' class='slick-prev slick-arrow'>
				<i class='fas fa-angle-left' aria-hidden='true'></i> <span>Previous Stop</span>
			</button>
			<button id="next-trail" type='button' class='slick-next slick-arrow'>
				<span>Next Stop</span> <i class='fas fa-angle-right' aria-hidden='true'></i>
			</button>
		</div>
	</div>
</div>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div id="post-wrap">
			<h2><?php echo single_cat_title(); ?></h2>
			<p class="go-explore">
				<a id="export-trail" class="radial radial--org radial--long" target="_blank" href="<?php echo get_field('google_places_list_url', $post->ID, false); ?>">
					<?php echo get_template_part('img/icons/inline','backpack_icon.svg'); ?>
					<span>Export Trail</span>
				</a>
			</p>
			<div class="content-wrap">
				<div class="inner">
			<?php        
				$cat_parent->slug === 'lake-erie-birding-trail' ? $listing = $trails : $listing = $trailStops;
				foreach ($listing as $listItem) :  setup_postdata($listItem);
		    ?>
                <div class="post">
					<div class="inner">
						<section>
							<span>
                    			<img src="<?php echo kdmfi_get_featured_image_src( 'featured-image-2', 'full' ); ?>" />
                			</span>
							<h2 class="item-title"><?php echo $listItem->post_title; ?></h2>
							<p class="item-address"><?php echo get_geocode_address( $listItem->ID ); ?></p>
						</section>
					</div>
				</div>
			<?php 
				endforeach;
			?> 
				</div>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php 
	endforeach;
	wp_reset_query(); 
	get_footer(); 
?>