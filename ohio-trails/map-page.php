<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.0
 * 
 * Template Name: Map-Page
 * Template Post Type: post, page, trail-stops
 */

get_header(); ?>

<div id="application" <?php if( isset($_GET['category']) && $_GET['category'] == "feature" ){ echo 'class="info"'; } ?>data-catid="<?php echo $category_id; ?>" data-post-tag="<?php echo $tagTarget; ?>" data-birding="<?php if(is_page('lake-erie-birding-trail')){ echo 'birding'; } ?>">
	<div id="mapCanvas"></div>
	<div id="featureWindow" class="<?php echo get_field( "the_season" ); if( isset($_GET['category']) && $_GET['category'] == "feature" ){ echo ' open'; } ?>">
		<button id="fwClose" class="closePanel" type="button"><span>close</span><i class="fas fa-times"></i></button>
		<?php include('featured-trails.php'); ?>
	</div>
	<div id="infoWindow" data-page-thumb="<?php echo get_the_post_thumbnail_url($thisPost_id->ID); ?>">
		<button id="iwClose" class="closePanel" type="button"><span>close</span><i class="fas fa-times"></i></button>
		<div class="carousel-wrap">
			<div id="carousel"></div>
		</div>
		<div class="btn-wrap">
			<button id="prev-trail" type='button' class='slick-prev slick-arrow'>
				<i class='fas fa-angle-left' aria-hidden='true'></i> <span>Previous Trail</span>
			</button>
			<button id="next-trail" type='button' class='slick-next slick-arrow'>
				<span>Next Trail</span> <i class='fas fa-angle-right' aria-hidden='true'></i>
			</button>
		</div>
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div id="post-wrap">
			<?php if( get_field('google_places_list_url', $post->ID) && !is_front_page() ){ ?>
				<h2><?php echo $post->post_title; ?></h2>
				<p class="go-explore">
					<a id="export-trail" class="radial radial--org radial--long" target="_blank" href="<?php get_field('google_places_list_url', $post->ID); ?>">
						<?php echo get_template_part('img/icons/inline','backpack_icon.svg'); ?>
						<span>Export Trail</span>
					</a>
				</p>
			<?php } ?>
			<div class="content-wrap">
				<div id="listFilters" class="<?php echo get_field( "the_season" ); ?>">
					<ul class="categoryFilter categoryFilter__listView">
						<li>
							<button class="filter-feature" type="button" data-category="feature">Featured</button>
						</li>
						<li>
							<button class="filter-spirits" type="button" data-category="spirits">Spirits</button>
						</li>
						<li>
							<button class="filter-coffee" type="button" data-category="coffee">Coffee</button>
						</li>
						<li>
							<button class="filter-food" type="button" data-category="food">Food</button>
						</li>
						<li>
							<button class="filter-shopping" type="button" data-category="shopping">Shopping</button>
						</li>
						<li>
							<button class="filter-sightseeing" type="button" data-category="sightseeing">Sights</button>
						</li>
						<li>
							<button class="filter-artmuseum" type="button" data-category="artmuseum">History</button>
						</li>
					</ul>
				</div>
				<div class="inner" id="list-bucket"></div>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->
			
<?php get_footer(); ?>