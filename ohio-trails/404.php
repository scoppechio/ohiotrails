<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="missing">
    <div>
        <p>Those who wander are not always lost, but you have strayed too far...</p>
        <p>
            <a href="<?php echo site_url(); ?>"> Get back to the Trails &raquo;</a>
        </p>
    </div>
</div>

<?php get_footer(); ?>