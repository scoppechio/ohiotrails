<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.2
 */

$openFilter;
$openSidebar;
$the_season = get_field('the_season');

if(!strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobile') || !strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android')) {
	if(isset($_GET['category']) && $_GET['category'] !== 'feature'){
		$openFilter = 'filter open';
		$openSidebar = 'open';
	}

	// https://stackoverflow.com/questions/6322112/check-if-php-page-is-accessed-from-an-ios-device
}

?>
	<footer id="colophon" class="site-footer" role="contentinfo">
	<?php if(!is_front_page()) { ?>
		<div id="ui-controls">
			<div class="pill-holder <?php echo $openFilter; ?>">
				<ul>
					<li>
						<button id="close-side" type="button">
							<i class="fas fa-times"></i>
						</button>
					</li>
					<li>
						<button id="map-toggle" type="button">
							<i class="fas fa-bars"></i>
							<span>List View</span>
						</button>
					</li>
					<?php if( basename(get_permalink()) == 'map') { ?>
					<li>
						<button id="filter-toggle" type="button">
							<i class="fas fa-filter"></i>
							<span>Filter</span>
						</button>
					</li>
					<?php } ?>
				</ul>
			</div>
			<div id="sidebar" class="<?php echo $openSidebar . ' ' . $the_season; ?>">
				<ul class="categoryFilter categoryFilter__sidebar">
					<li>
						<button class="filter-feature" data-category="feature" type="button">
							<span>Featured <?php echo $the_season; ?> Trails</span>
							<span class="marker"></span>
						</button>
					</li>
					<li>
						<button class="filter-spirits" data-category="spirits" type="button">
							<span>Spirits</span>
							<span class="marker"></span>
						</button>
					</li>
					<li>
						<button class="filter-coffee" data-category="coffee" type="button">
							<span>Coffee</span>
							<span class="marker"></span>
						</button>
					</li>
					<li>
						<button class="filter-food" data-category="food" type="button">
							<span>Food</span>
							<span class="marker"></span>
						</button>
					</li>
					<li>
						<button class="filter-shopping" data-category="shopping" type="button">
							<span>Shopping</span>
							<span class="marker"></span>
						</button>
					</li>
					<li>
						<button class="filter-sightseeing" data-category="sightseeing" type="button">
							<span>Sights</span>
							<span class="marker"></span>
						</button>
					</li>
					<li>
						<button class="filter-history" data-category="artmuseum" type="button">
							<span>History</span>
							<span class="marker"></span>
						</button>
					</li>
				</ul>
				<div id="side-nav">
					<ul>
						<li>
							<a href="http://ohiodnr.gov/" target="_blank">Visit Natural Trails</a>
						</li>
					</ul>
				</div>
			</div>
		
			<!--
			<button title="Category Filter" class="radial radial--blu" type="button"><i class="fas fa-ellipsis-v"></i></button>
			<button title="List View"  class="radial radial--rog" type="button"><i class="fas fa-list-ul"></i></button>
			-->
			<?php //get_search_form(); ?>
		</div>
	<?php } ?>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
