<?php
/*
* Template Name: The Scoop
*/

global $post;

/////=======================================================================

$catIn = array(
	'category_name' => "ohio-ice-cream-trail",
	'post_type' => 'trail-stops',
	'numberposts' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
);
	
$category = get_posts($catIn);

$blog_category = array(
	'category_name' => 'ice-cream-blog',
	'numberposts' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
);
$blog_posts = get_posts($blog_category);

/////=======================================================================

// var_dump($category);

get_header(); ?>

<div id="primary">
	<div id="hero"></div>
	<main id="main" class="content-wrap site-main" role="main">
		<span id="ohio-sticky"></span>
		<div class="main-top">
			<div>
				<h2>Ohio Ice Cream Trail</h2>
				<p class="stops"><?php echo count($category).' stops'; ?></p>
				<p class="trail-content"><?php echo $post->post_content; ?></p>
			</div>
			<div class="trail-actions">
				<p><a class="get-it radial radial--org" target="_blank" href="http://trails.ohio.org/wp-content/uploads/2018/06/Ohio-Ice-Cream-Trail-Map.pdf">Download Map</a></p>
				<p><a class="see-it radial radial--blu" href="<?php echo site_url('/ohio-trails/ohio-ice-cream-trail/'); ?>">Interactive Map</a></p>
			</div>
		</div>
		<div class="socials">
			<ul>
				<li><p>Share this Trail:</li>
				<li>
					<a target="_blank" href="https://www.facebook.com/sharer.php?u=http%3A%2F%2Ftrails.ohio.org%2Fice-cream%2F&hashtag=%23MyOhioScoop"><img src="<?php echo get_stylesheet_directory_uri().'/img/icons/social-fb.png'; ?>" /></a>
				</li>
				<li>
					<a target="_blank" href="https://www.instagram.com/ohio.findithere/"><img src="<?php echo get_stylesheet_directory_uri().'/img/icons/social-ig.png'; ?>" /></a>
				</li>
				<li>
					<a class="twitter-share-button" target="_blank" href="https://twitter.com/share?
					text=Share%20your%20%23MyOhioScoop%20story%21&
					url=http%3A%2F%2Ftrails.ohio.org%2Fice-cream%2F&
					hashtags=MyOhioScoop"><img src="<?php echo get_stylesheet_directory_uri().'/img/icons/social-tw.png'; ?>" /></a>
				</li>
			</ul>
		</div>
		<!-- <p class="vote-cta-wrap"><button type="button" class="radial radial--red" id="go-vote">Vote for the next stop!</button></p> -->
	</main>
<!--
	<div id="ss_import">
	-- Prod embed code --
	<iframe class='embed_frame' src='https://a.pgtb.me/59dvWj?embed=1&vOffset=0&autoscroll_p=1' seamless='seamless' frameBorder='0' scrolling=no style='display: none' allowFullScreen></iframe> <script src='https://d1m2uzvk8r2fcn.cloudfront.net/scripts/embed-code/1528391666.min.js' type='text/javascript'></script>
	-- Dev embed code --
	-- <iframe class='embed_frame' src='https://a.pgtb.me/xlB3NL?embed=1&vOffset=0&autoscroll_p=1' seamless='seamless' frameBorder='0' scrolling=no style='display: none' allowFullScreen></iframe> <script src='https://d1m2uzvk8r2fcn.cloudfront.net/scripts/embed-code/1528391666.min.js' type='text/javascript'></script> --
	</div>
-->
	<main class="content-wrap">
		<div class="post-wrap">
			<h2>Current Ohio Ice Cream Trail</h2>
		<?php foreach ($category as $item) :  setup_postdata($item); ?>
			<!-- article -->
			<article id="post-<?php echo $item->ID; ?>" <?php post_class(); ?>>
				<div>
					<?php if(get_the_post_thumbnail_url($item->ID)){
						echo '<span style="background-image:url('.get_the_post_thumbnail_url($item->ID).')"></span>';
					} else {
						echo '<span style="background-image:url('.get_stylesheet_directory_uri().'/img/cone-pattern.jpg)"></span>';
					} ?>
				</div>
				<div>
					<h4 class="rating"></h4>
					<h2>
						<span class="item-post"><?php echo $item->post_title; ?></span>
						<span class="item-www">
							<?php if( get_post_meta($item->ID, 'Stop Website', true) ){ ?>
							<a title="<?php echo get_post_meta($item->ID, 'Stop Website', true); ?>" href="<?php echo get_post_meta($item->ID, 'Stop Website', true); ?>"></a>
							<?php } ?>
						</span>
					</h2>
					<h3 class="item-address"><?php echo get_geocode_address( $item->ID ); ?></h3>
					<span class="divider"></span>
					<p><?php echo $item->post_content; ?></p>
				</div>
			</article>
		<!-- /article -->
		<?php endforeach; ?>
		</div>
		</main>

		<div class="ice-cream-blog">
			<div class="content-wrap">
				<div class="ice-cream-blog">
					<h2>The Scoop</h2>
					<p class="blog-description"></p>
					<?php foreach ($blog_posts as $post) :  setup_postdata($post); ?>
					<article>
						<span class="divider"></span>
						<h3><?php echo $post->post_title; ?></h3>
						<p class="blog-date"><?php echo get_the_date('n/j/y'); ?></p>
						<div class="blog-contents">
							<?php if(get_the_post_thumbnail_url($post->ID)){
								echo '<span class="blog-image" style="background-image:url(' . get_the_post_thumbnail_url($post->ID) . ')"></span>';
							} ?>
							<div>
								<p><?php echo $post->post_content; ?></p>
								<a href="<?php echo get_post_meta($post->ID, 'Blog URL', true); ?>" target="_blank">Read More</a>
							</div>
						</div>
					</article>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<main class="content-wrap">
			<div class="main-bottom">
				<div class="flex-wrap">
					<div>
						<h2>Ohio Ice Cream Trail</h2>
					</div>
					<div class="trail-actions">
						<p><a class="get-it radial radial--org" target="_blank" href="http://trails.ohio.org/wp-content/uploads/2018/06/Ohio-Ice-Cream-Trail-Map.pdf">Download Map</a></p>
						<p><a class="see-it radial radial--blu" href="<?php echo site_url('/ohio-trails/ohio-ice-cream-trail/'); ?>">Interactive Map</a></p>
					</div>
				</div>
				<div class="socials">
					<ul>
						<li>
							<a target="_blank" href="http://www.facebook.com/OhioFindItHere"><img src="<?php echo get_stylesheet_directory_uri().'/img/icons/social-fb.png'; ?>" /></a>
						</li>
						<li>
							<a target="_blank" href="https://www.instagram.com/ohio.findithere/"><img src="<?php echo get_stylesheet_directory_uri().'/img/icons/social-ig.png'; ?>" /></a>
						</li>
						<li>
							<a target="_blank" href="http://twitter.com/OhioFinditHere"><img src="<?php echo get_stylesheet_directory_uri().'/img/icons/social-tw.png'; ?>" /></a>
						</li>
					</ul>
				</div>
			</div>
		</main>
</div><!-- #primary -->

<?php get_footer();