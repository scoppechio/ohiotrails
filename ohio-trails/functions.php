<?php
// CPT Finder
function is_post_type($type){
    global $wp_query;
    return $type == get_post_type($wp_query->post->ID);
}

// Our custom post type function
function create_posttype() {
 
    register_post_type( 'trail-head',
    // CPT Options
        array(
            'hierarchical' => true,
            'labels' => array(
                'name' => __( 'Trail Heads' ),
                'singular_name' => __( 'Trail Head' )
            ),
            'public' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => '/'),
            'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail', 'page-attributes' ),
            'taxonomies' => array('category', 'post_tag') // this is IMPORTANT
        )
    );

    register_post_type( 'trail-stops',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Trail Stops' ),
                'singular_name' => __( 'Trail Stop' )
            ),
            'public' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => '/'),
            'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail', 'page-attributes' ),
            'taxonomies' => array('category', 'post_tag') // this is IMPORTANT
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

// Multiple post images - Copy Pasta as needed
//
// kdmfi_the_featured_image( 'featured-image-2', 'full' );
add_filter( 'kdmfi_featured_images', function( $featured_images ) {
    $args = array(
      'id' => 'featured-image-2',
      'desc' => 'Container for setting the default loaded map icon for this trail',
      'label_name' => 'Default Map Icon',
      'label_set' => 'Set Default Map Icon',
      'label_remove' => 'Remove Default Map Icon',
      'label_use' => 'Set Default Map Icon',
      'post_type' => array( 'trail-head', 'trail-stops' ),
    );
  
    $featured_images[] = $args;
  
    return $featured_images;
});

add_filter( 'kdmfi_featured_images', function( $featured_images ) {
    $args = array(
      'id' => 'featured-image-3',
      'desc' => 'Trail List View image',
      'label_name' => 'List View Image',
      'label_set' => 'Set List View image',
      'label_remove' => 'Remove List View image',
      'label_use' => 'Set List View image',
      'post_type' => array( 'trail-head' ),
    );
  
    $featured_images[] = $args;
  
    return $featured_images;
});

add_theme_support( 'post-thumbnails' ); 

function trails_enqueue_styles(){
    wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/js/slick/slick.css' );
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/style.css' );
} 

function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
}
add_action( 'wp_enqueue_scripts', 'trails_enqueue_styles' );
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

function fa_loading() {
    wp_enqueue_style( 'font-awesome-free', '//use.fontawesome.com/releases/v5.2.0/css/all.css' );
}
add_action( 'wp_enqueue_scripts', 'fa_loading' );

function register_jquery() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js');
    wp_enqueue_script( 'jquery' );
    
    wp_register_script( 'jqueryUi', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js');
    wp_enqueue_script( 'jqueryUi' );
    
    wp_register_script( 'slick', get_stylesheet_directory_uri().'/js/slick/slick.min.js');
    wp_enqueue_script( 'slick' );
}     
add_action('wp_enqueue_scripts', 'register_jquery');

function register_local_scripts() {
    wp_enqueue_script( 'globals', get_stylesheet_directory_uri() . '/js/globals.js', array ( 'jquery' ), 1.1, true );
    wp_enqueue_script( 'functions', get_stylesheet_directory_uri() . '/js/functions.js', array ( 'jquery' ), 1.1, true );
    wp_enqueue_script( 'map', get_stylesheet_directory_uri() . '/js/map.js', array ( 'jquery' ), 1.1, true );
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true );

    $siteVars = array(
      'templateUrl'   => get_stylesheet_directory_uri(),
      'siteUrl'       => site_url()
    );
    wp_localize_script( 'globals', 'siteVars', $siteVars );
}
add_action( 'wp_enqueue_scripts', 'register_local_scripts' );

function register_google_maps_api() {
    if(!is_front_page()) {
        $gMapAPI = 'AIzaSyDGF2tq24HytYvu68L7Mpcnpog-1u1MAFo';
        // AIzaSyD6chSZy1akReOeE_FkcPJqSl8FvJ1dUwI
        wp_register_script( 'gMap', 'https://maps.googleapis.com/maps/api/js?key='.$gMapAPI.'&libraries=places&callback=initMap', array(), null, true );
        wp_enqueue_script( 'gMap' );

        wp_register_script( 'markerCluster', 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js', array( 'gMap' ), null, true);
        wp_enqueue_script( 'markerCluster' );
    }
}
add_action('wp_enqueue_scripts', 'register_google_maps_api');

function register_landing() {
    wp_register_script( 'tween', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js' );
    wp_enqueue_script( 'tween' );
    
    if(is_front_page()) {
        wp_register_script( 'tween', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js' );
        wp_register_script( 'scrollOverflow', get_stylesheet_directory_uri() . '/js/fullpage/scrolloverflow.min.js', array ( 'jquery' ), 1.1, true );
        wp_register_script( 'fp-extend', get_stylesheet_directory_uri() . '/js/fullpage/fullpage.extensions.min.js', array ( 'jquery' ), 1.1, true );

        wp_register_script( 'snow-fall', get_stylesheet_directory_uri() . '/js/snow.js', array ( 'jquery' ), 1.1, true );
        wp_register_script( 'full-page', get_stylesheet_directory_uri() . '/js/fullpage/fullpage.js', array ( 'jquery' ), 1.1, true );
        wp_register_script( 'landing-script', get_stylesheet_directory_uri() . '/js/landing-script.js', array ( 'jquery' ), 1.1, true );
        wp_enqueue_script( 'scrollOverflow' );
        wp_enqueue_script( 'fp-extend' );
        //wp_enqueue_script( 'full-page' );
        wp_enqueue_script( 'snow-fall' );
        wp_enqueue_script( 'landing-script' );
    }
}
add_action( 'wp_enqueue_scripts', 'register_landing' );

function register_featured() {
    // wp_register_style( 'featured-style', get_stylesheet_directory_uri() . '/featured-trails/featured-style.css' );
    // wp_enqueue_style( 'featured-style' );

    // wp_register_script( 'featured-script', get_stylesheet_directory_uri() . '/featured-trails/featured-script.js' );
    // wp_enqueue_script( 'featured-script' );
}
add_action( 'wp_enqueue_scripts', 'register_featured' );

// Ex Post add Async and Defer
function my_async_scripts( $tag, $handle, $src ) {
    // the handles of the enqueued scripts we want to async
    $async_scripts = array( 'gMap' );

    if ( in_array( $handle, $async_scripts ) ) {
        return '<script type="text/javascript"  async defer src="' . $src . '"></script>' . "\n";
    }

    return $tag;
}
add_filter( 'script_loader_tag', 'my_async_scripts', 10, 3 );

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

function prefix_conditional_body_class( $classes ) {
    if( is_page_template('map-page.php') )
        $classes[] = 'map-page';

    return $classes;
}
add_filter( 'body_class', 'prefix_conditional_body_class' );

add_action( 'admin_enqueue_scripts', 'my_ajax_scripts' );
function my_ajax_scripts() {
    wp_localize_script( 'ajaxRequestId', 'ajaxurl', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

function wpb_change_search_url() {
    if ( is_search() && ! empty( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }   
}
add_action( 'template_redirect', 'wpb_change_search_url' );

add_filter( 'category_rewrite_rules', 'vipx_filter_category_rewrite_rules' );
function vipx_filter_category_rewrite_rules( $rules ) {
    $categories = get_categories( array( 'hide_empty' => false ) );

    if ( is_array( $categories ) && ! empty( $categories ) ) {
        $slugs = array();
        foreach ( $categories as $category ) {
            if ( is_object( $category ) && ! is_wp_error( $category ) ) {
                if ( 0 == $category->category_parent ) {
                    $slugs[] = $category->slug;
                } else {
                    $slugs[] = trim( get_category_parents( $category->term_id, false, '/', true ), '/' );
                }
            }
        }

        if ( ! empty( $slugs ) ) {
            $rules = array();

            foreach ( $slugs as $slug ) {
                $rules[ '(' . $slug . ')/feed/(feed|rdf|rss|rss2|atom)?/?$' ] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
                $rules[ '(' . $slug . ')/(feed|rdf|rss|rss2|atom)/?$' ] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
                $rules[ '(' . $slug . ')(/page/(\d)+/?)?$' ] = 'index.php?category_name=$matches[1]&paged=$matches[3]';
            }
        }
    }
    return $rules;
}

function fetch_category() {
    if( is_page_template( 'category-nav.php' ) ) {
        global $wp;
        $url = home_url( $wp->request );
        $tokens = explode('/', $url);
        $route =  $tokens[sizeof($tokens)-1];

        wp_localize_script( 'map', 'fetch', array(
                'madeit' => 'yes',
                'urlRoute' => $route
            )
        );
        
        return $route;
    }
}
add_action( 'wp_enqueue_scripts', 'fetch_category' );

?>