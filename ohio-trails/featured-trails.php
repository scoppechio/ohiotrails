<?php
  // load wordpress into template
  define('WP_USE_THEMES', false);
  require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
    
  $the_season = get_field('the_season');
  //get featured trails
  $featured_args = array(
      'post_type' => 'trail-head',
      'tax_query' => array(
          array(
              'taxonomy' => 'post_tag',
              'field' => 'slug',
              'terms' => 'feature'
          )
      ),
      'posts_per_page' => -1
  );

  // The Query
  $the_query = query_posts( $featured_args );
?>

<div class="inner">
  <h4><strong>-</strong> Feature <strong>-</strong></h4>
  <h2 class="feature-title">Featured <?php echo $the_season; ?> Trails</h2>
  <h3 class="trail-count"><?php echo count($the_query).' Trails'; ?></h3>
  <p class="ft-btn-wrap">
    <button id="view-feature">View Trails</button>
  </p>
  <?php
    if( have_rows('intro_copy') ):
      while ( have_rows('intro_copy') ) : the_row();
        if( get_sub_field('season_select') == $the_season ){
          the_sub_field('season_text');
        }
      endwhile;      
    endif;
  ?>
</div>
<?php 
  if( $the_season == 'winter' ){
    echo '<canvas id="snow-canvas"></canvas>';
  }
?>