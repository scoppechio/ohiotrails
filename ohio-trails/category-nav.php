<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.0
 * 
 * Template Name: Category Link
 * Template Post Type: post, page, trail-stops
 */
get_header(); 

$route = fetch_category();
?>

<div id="trail-home">
	<a href="<?php  echo home_url(); ?>"><i class="fas fa-angle-left"></i> Trails</a> / <span><?php echo $route .' Trails' ;?></span>
</div>

<div id="application" data-post-tag="<?php echo $route; ?>">
	<div id="mapCanvas"></div>
	<div id="infoWindow" data-page-thumb="<?php echo get_the_post_thumbnail_url($thisPost_id->ID); ?>">
		<button id="iwClose" class="closePanel" type="button"><span>close</span><i class="fas fa-times"></i></button>
		<div class="carousel-wrap">
			<div id="carousel"></div>
		</div>
		<div class="btn-wrap">
			<button id="prev-trail" type='button' class='slick-prev slick-arrow'>
				<i class='fas fa-angle-left' aria-hidden='true'></i> <span>Previous <?php if(!is_front_page('home')) { echo 'Stop'; } else { echo 'Trail'; } ?></span>
			</button>
			<button id="next-trail" type='button' class='slick-next slick-arrow'>
				<span>Next <?php if(!is_front_page('home')) { echo 'Stop'; } else { echo 'Trail'; } ?></span> <i class='fas fa-angle-right' aria-hidden='true'></i>
			</button>
		</div>
	</div>
</div>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div id="post-wrap">
			<?php if( get_field('google_places_list_url', $post->ID) && !is_front_page() ){ ?>
				<h2><?php echo $post->post_title; ?></h2>
				<p class="go-explore">
					<a id="export-trail" class="radial radial--org radial--long" target="_blank" href="<?php get_field('google_places_list_url', $post->ID); ?>">
						<?php echo get_template_part('img/icons/inline','backpack_icon.svg'); ?>
						<span>Export Trail</span>
					</a>
				</p>
			<?php } ?>
			<div class="content-wrap">
                <div>
                    <h2 class="cat-post-header"><?php echo $route .' Trails' ;?></h2>
                </div>
				<div class="inner" id="list-bucket"></div>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>