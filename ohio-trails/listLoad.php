<?php
    // load wordpress into template
    define('WP_USE_THEMES', false);
    require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

    global $post;
    
    if(isset($_GET['tag'])){    
        $tag_name = $_GET['tag'];

        $query  = explode('&', $_SERVER['QUERY_STRING']);
        $params = array();

        foreach( $query as $param )
        {
            list($name, $value) = explode('=', $param, 2);
            $params[urldecode($name)][] = urldecode($value);
        }

        $findHeads = array(
            'post_type' => 'trail-head',
            'tag_slug__in' => $params['tag'],
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        );
    } else {
        $findHeads = array(
            'post_type' => 'trail-head',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        );
    }

    $trailHeads = query_posts($findHeads);
    
    foreach ($trailHeads as $post) :  setup_postdata($post);
        $findStops = array(
            'post_type' => 'trail-stops',
            'category_name' => $post->post_name,
            'posts_per_page' => -1
        );
        $trailStops = query_posts($findStops);    
?>
<div class="post">
    <div class="inner post-link <?php if($post->post_name == 'cache-4-coins-geotour'){ echo 'post-link--override'; } ?>">
        <button class="lazy" type="button" data-post-id="<?php echo $post->ID; ?>" data-img="<?php echo kdmfi_get_featured_image_src( 'featured-image-3', 'full' ); ?>">
            <div>
                <span>
                    <img src="<?php echo kdmfi_get_featured_image_src( 'featured-image-2', 'full' ); ?>" />
                </span>
            </div>
        </button>
        <section>
        <h2><?php the_title(); ?></h2>
        <p><?php echo count($trailStops); ?> Stops</p>
        
        <div class="list-foot">
            <?php if(get_field('google_places_list_url', $post->ID)){ ?>
                <p class="go-explore">
                    <a id="export-trail-<?php echo $post->ID; ?>" class="listExport radial radial--org radial--long" target="_blank" href="<?php echo get_field('google_places_list_url', $post->ID, false); ?>">
                        <?php echo get_template_part('img/icons/inline','backpack_icon.svg'); ?>
                        <span>Export Trail</span>
                    </a>
                </p>
            <?php } ?>
            <?php if(count($trailStops) > 0) { ?>
                <p class="post-link <?php if( $post->post_name == 'cache-4-coins-geotour' || !get_field('google_places_list_url', $post->ID) ){ echo 'post-link--override'; } ?>"><button id="view-trail-<?php echo $post->ID; ?>" type="button" data-post-id="<?php echo $post->ID; ?>" class="listViewTrail radial radial--blu radial--long">View Trail</button></p>
            <?php } ?>
        </div>
    </section>
    </div>
</div>
<?php 
    endforeach;
    wp_reset_query();  
?>