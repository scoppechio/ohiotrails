<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.0
 * 
 * Template Name: Winter Landing
 * Template Post Type: post, page, trail-stops
 */

get_header(); 

    //get featured trails
    $featured_args = array(
        'post_type' => 'trail-head',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'feature'
            )
        ),
        'posts_per_page' => -1,
        'order' => 'ASC'
    );

    // The Query
    $the_query = query_posts( $featured_args );
?>
<div id="carousel-wrap" class="winter">
    <div id="controls-header">
        <div class="btn-wrap">
            <a href="<?php echo home_url('map'); ?>">
                <img alt="view map" src="<?php echo get_stylesheet_directory_uri() . '/featured-trails/assets/view-map-tab.svg' ?>" />
            </a>
        </div>
        <div class="logo-wrap">
            <img alt="Trails Logo" src="<?php echo get_stylesheet_directory_uri() . '/featured-trails/assets/ohio-trails.png' ?>" />
            <span>Featured Winter Trails</span>
        </div>
    </div>
    <canvas id="snow-canvas"></canvas>
    <div id="track">
    <?php if(get_field('show_intro_section')) { ?>
        <div class="section panel">
            <div class="inner inner--first">
                <div class="content">
                    <h1 title="<?php echo $post->post_title; ?>"><img alt="Trails Logo Tall"src="<?php echo get_stylesheet_directory_uri() . '/landing-page/assets/initial-adventure-logo.svg' ?>" /></h1>
                    <div class="copy">
                        <?php 
                            if( have_rows('intro_copy') ):
                                while( have_rows('intro_copy') ) : the_row();
                                    if( get_sub_field('season_select') == 'winter'):
                                        the_sub_field('season_text');
                                    endif;                                    
                                endwhile;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if(get_field('show_trails_logo_section')) { ?>
            <div class="section panel">
                <div class="inner inner--second">
                    <div class="content">
                        <div class="logo"><img alt="Trails Logo" class="logo" src="<?php echo get_stylesheet_directory_uri() . '/landing-page/assets/trails-ohio-find-it-here-v-rev.png' ?>"></div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if(get_field('show_featured_trail_section')) { ?>
            <?php
                foreach($the_query as $post){ 
                    $catTrail = array(
                        'post_type'=> 'trail-stops',
                        'category_name' => $post->post_name,
                        'posts_per_page'=> -1
                    );
                    $findMatch = get_posts( $catTrail );
                    $num_trails = 0;
                    if( isset( $findMatch ) )
                        $num_trails = count( $findMatch );
            ?>
                <div class="section panel loop">
                    <div class="inner inner--overflow">
                        <span class="bg-blur" data-bgimg="<?php echo kdmfi_get_featured_image_src( 'featured-image-3', 'full' ); ?>"></span>
                        <div class="ft-container">
                            <div class="inner">
                                <div class="lt">
                                    <span class="sq-thumb" data-thumb="<?php echo kdmfi_get_featured_image_src( 'featured-image-3', 'full' ); ?>"></span>
                                </div>
                                <div class="rt">
                                    <img alt="Map Icon" src="<?php echo kdmfi_get_featured_image_src( 'featured-image-2', 'full' ); ?>" class="mini-icon">
                                    <h2 class="content-label-main"><?php echo get_the_title();?></h2> 
                                    <p class="stop-text">
                                        <?php echo "$num_trails stop" ;  if( $num_trails == 0 || $num_trails > 1 ) { echo 's'; }?>
                                    </p>
                                    <hr class="content-line-break">
                                    <p class="content-text">
                                        <?php echo $post->post_content; ?>
                                    </p>
                                    <div class="btn-wrap">
                                        <a href="<?php echo get_the_permalink(); ?>" target="_blank">View Trail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="section panel">
            <div class="inner inner--last">
                <div class="start-exploring-bg">
                    <div class="se-row">
                        <img alt="Trails Logo" src="<?php echo get_stylesheet_directory_uri() . '/featured-trails/assets/ohio-trails.png' ?>" />
                    </div>
                    <div class="se-row">
                        <img alt="dots flourish" src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/dotted-trail-left.png' ?>" />
                        <img alt="clouds" id="cloud-left" src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/cloud-3.png' ?>" />
                        <a id="show-map" class="pulse" href="<?php echo home_url('map'); ?>" title="Start Exploring">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/start-exploring-map.png' ?>" />
                            <span>Start Exploring</span>
                        </a>
                        <img alt="clouds" id="cloud-right" src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/cloud-1.png' ?>" />
                        <img alt="dots flourish" src="<?php echo get_stylesheet_directory_uri() . '/start-exploring/assets/dotted-trail-right.png' ?>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="scroll-wrap">
    <span class="line"></span>
    <span class="dot"></span>
    <span class="text">Scroll</span>
</div>

<?php wp_footer(); ?>