<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Ohio_dot_org
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php 
		if( is_category() ) {
			$category = get_query_var('cat');
			$current_cat = get_category($cat);

			$findHead = array(
				'post_type' => 'trail-head',
				'name' => $current_cat->slug,
				'posts_per_page' => -1
			);
			$trailHead = query_posts($findHead);
			
			foreach ($trailHead as $post) :  setup_postdata($post);
	?>
	<!-- Place this data between the <head> tags of your website -->
	<title><?php echo single_post_title(); ?> - Ohio Adventure Trails - Ohio. Find It</title>
	<meta name="description" content="<?php echo wp_strip_all_tags($post->post_content); ?>" />
	
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="<?php echo wp_strip_all_tags($post->post_content); ?>">
	
	<!-- Open Graph data -->
	<meta property="og:title" content="<?php echo single_post_title(); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo home_url( $post->post_name ); ?>" />
	<meta property="og:image" content="<?php 
		if(is_front_page('home')){
			echo content_url('/img/preview-thumb.jpg');
		} else {
			echo get_the_post_thumbnail_url($item->ID);
		} ?>" />
	<meta property="og:description" content="<?php echo wp_strip_all_tags($post->post_content); ?>" />

	<?php 
		endforeach; 
		} else {
	?>
	
	<!-- Place this data between the <head> tags of your website -->
	<title><?php echo single_post_title(); ?> - Ohio Adventure Trails - Ohio. Find It</title>
	<meta name="description" content="<?php echo wp_strip_all_tags($post->post_content); ?>" />
	
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="<?php echo wp_strip_all_tags($post->post_content); ?>">
	
	<!-- Open Graph data -->
	<meta property="og:title" content="<?php echo single_post_title(); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo home_url( $post->request ); ?>" />
	<meta property="og:image" content="<?php 
		if(is_front_page('home')){
			echo content_url('/img/preview-thumb.jpg');
		} else {
			echo get_the_post_thumbnail_url($item->ID);
		} ?>" />
	<meta property="og:description" content="<?php echo wp_strip_all_tags($post->post_content); ?>" />

	<?php } ?>

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri().'/apple-touch-icon.png'; ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri().'/favicon-32x32.png'; ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri().'/favicon-16x16.png'; ?>">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri().'/site.webmanifest'; ?>">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri().'/safari-pinned-tab.svg'; ?>" color="#9d2235">
	<meta name="msapplication-TileColor" content="#febd3b">
	<meta name="theme-color" content="#ffffff">

	<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
	<script>var dataLayer = new Array();</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NMFZ6W9');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMFZ6W9" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="page" class="site <?php if($_COOKIE['visit_'] || !is_front_page('home')){ echo 'gingerbread'; } ?>">
	<?php if(!is_front_page('home') ) { ?>
		<header id="masthead" class="site-header" role="banner">
			<div class="">
				<a id="ohio-logo" href="<?php echo get_site_url().'/map'; ?>">
					<img id="logo-img" src="<?php echo get_stylesheet_directory_uri().'/img/FPO_logo.png'; ?>" />
				</a>
				<div class="navigation-top">
					<a id="ohio-home" href="http://www.ohio.org/" target="_blank">
						<span>ohio.org</span>
					</a>
					<!-- <button type="button" id="delCookie">Delete Visit cookie</button> -->
					<?php // wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_id' => 'header_nav' ) ); ?>
				</div>
			</div>
		</header><!-- #masthead -->
	<?php } ?>
	<?php if(is_page_template('map-page.php') || !is_category()) { ?>
		<div id="page-load"><span><i class="fas fa-spinner fa-spin"></i></span></div>
	<?php } ?>
