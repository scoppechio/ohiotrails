<?php
    // load wordpress into template
    define('WP_USE_THEMES', false);
    require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

    // Start XML file, create parent node
    $dom = new DOMDocument("1.0");
    $node = $dom->createElement("markers");
    $parnode = $dom->appendChild($node);

    // The Query    
    if( isset($_GET['category_id']) )
    {
        // Trail location pages - getting the location's category 
        $category_id = $_GET['category_id'];
        $category_name = $_GET['category'];
        $allPosts = query_posts(array(
            'category__in' => $category_id,
            'tag_slug__in' => $category_name,
            'post_type' => 'trail-stops',
		    'posts_per_page' => -1,
		    'orderby' => 'title',
		    'order' => 'ASC'
        ));
    } else if( isset($_GET['category']) ) {
        // Trail Sorting/Filters
        $category_name = $_GET['category'];
        $query  = explode('&', $_SERVER['QUERY_STRING']);
        $params = array();

        foreach( $query as $param )
        {
            list($name, $value) = explode('=', $param, 2);
            $params[urldecode($name)][] = urldecode($value);
        }
        $allPosts = query_posts(array(
            'tag_slug__in' => $params['category'],
            'post_type' => 'trail-head',
            'posts_per_page' => -1,
            'meta_key' => 'carousel_order',
            'orderby' => 'meta_value title',
            'order' => 'ASC'
        ));
    } else {
        // Default Load
        $allPosts = query_posts(array(
            'post_type' => 'trail-head',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        ));
    }

    function markerIcon(){
        if($params['category']){
            foreach( $params['category'] as $searchTag ){
                if(count($params['category']) > 1){
                    $iconURL = kdmfi_get_featured_image_src( 'featured-image-2', 'full' );
                } else {
                    if( in_array($searchTag, $params['category'])) { 
                        $iconURL = get_template_directory_uri().'/img/icons/pin-'. $searchTag .'.svg'; 
                    } else {
                        $iconURL = kdmfi_get_featured_image_src( 'featured-image-2', 'full' );
                    }
                }
            }
        } else {
            $iconURL = kdmfi_get_featured_image_src( 'featured-image-2', 'full' );
        }

        return $iconURL;
    }

    header("Content-type: text/xml");
    
    foreach($allPosts as $post)
    {
        $tags = wp_get_post_tags($post->ID);
        
        $this_tag = array_map(function ($object) { return $object->slug; }, $tags);
        $these_tags =  implode(', ', $this_tag);

        $iconURL = markerIcon();

        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        
        $newnode->setAttribute("id", $post->ID);
        $newnode->setAttribute("title", $post->post_title);
        $newnode->setAttribute("page", $post->post_name);
        $newnode->setAttribute("address", get_geocode_address( $post->ID ));
        $newnode->setAttribute("lat", get_geocode_lat( $post->ID ));
        $newnode->setAttribute("lng", get_geocode_lng( $post->ID ));
        $newnode->setAttribute("category", $these_tags);
        $newnode->setAttribute("iconUrl", $iconURL);
    }
    
    echo $dom->saveXML();
    
?>