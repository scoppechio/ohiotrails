var ohioLight = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            { "color": "#6195a0" }
        ]
        
    },
    {
        "featureType": "administrative.province",
        "elementType": "geometry.stroke",
        "stylers": [
            { "visibility": "off" }
        ]
        
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels",
        "stylers": [
            { "lightness": "40" }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            { "lightness": "100" },
            { "saturation": "100" },
            { "gamma": "1.00" },
            { "color": "#f2f2f2" }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            { "lightness": "-3" },
            { "gamma": "1.00" }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "all",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "hue": "#00c2ff"},
            { "lightness": "23" }
        ]
    },
    {
        "featureType": "poi.government",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "off" },
            { "color": "#d70707" }
        ]
    },
    {
        "featureType": "poi.medical",
        "elementType": "geometry.fill",
        "stylers": [
            { "visibility": "on" },
            { "hue": "#00ebff" },
            { "saturation": "-29" },
            { "lightness": "43" }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            { "color": "#bed145" },
            { "visibility": "on" },
            { "lightness": "60" }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            { "saturation": -100 },
            { "lightness": 45 },
            { "visibility": "simplified" }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            { "color": "#e98f3a" },
            { "visibility": "simplified" },
            { "saturation": "100" },
            { "lightness": "70" }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text",
        "stylers": [
            { "color": "#4e4e4e" }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
            { "color": "#787878" }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            { "visibility": "simplified" }
        ]
    },
    {
        "featureType": "transit.station.airport",
        "elementType": "labels.icon",
        "stylers": [
            { "hue": "#0a00ff" },
            { "saturation": "-77" },
            { "gamma": "0.57" },
            { "lightness": "0" }
        ]
    },
    {
        "featureType": "transit.station.rail",
        "elementType": "labels.text.fill",
        "stylers": [
            { "color": "#43321e" }
        ]
    },
    {
        "featureType": "transit.station.rail",
        "elementType": "labels.icon",
        "stylers": [
            { "hue": "#ff6c00" },
            { "lightness": "4" },
            { "gamma": "0.75" },
            { "saturation": "-68" }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            { "color": "#eaf6f8" },
            { "visibility": "on" }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            { "color": "#79c7ce" },
            { "lightness": "50" }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            { "lightness": "-49" },
            { "saturation": "-53" },
            { "gamma": "0.79" }
        ]
    }
];

function initMap() {
    
    iw = new google.maps.InfoWindow();
    bounds = new google.maps.LatLngBounds();
	map = new google.maps.Map(document.getElementById('mapCanvas'), {
		zoom: 12,
		maxZoom: 15,
		center: mapCenter,
		streetViewControl: false,
		gestureHandling: 'greedy',
		styles: ohioLight,
		disableDefaultUI: true
    });
	
	var world = [{lng: 0, lat: 90,}, {lng: -180, lat: 90}, {lng: -180, lat: 0}, {lng: 0, lat: 0}];

	// Ohio Polygon Coords
	var ohioBounds = [{lng: -80.51904, lat: 41.22158}, {lng: -80.51887, lat: 41.22051}, {lng: -80.51889, lat: 41.18818}, {lng: -80.51904, lat: 41.12499}, {lng: -80.51896, lat: 41.06155}, {lng: -80.519, lat: 41.00985}, {lng: -80.51902, lat: 40.96092}, {lng: -80.51979, lat: 40.90232}, {lng: -80.51905, lat: 40.86955}, {lng: -80.51909, lat: 40.80787}, {lng: -80.51905, lat: 40.76627}, {lng: -80.51893, lat: 40.72383}, {lng: -80.51899, lat: 40.65321}, {lng: -80.51899, lat: 40.6388}, {lng: -80.57018, lat: 40.61628}, {lng: -80.64068, lat: 40.54795}, {lng: -80.62345, lat: 40.39711}, {lng: -80.67905, lat: 40.18683}, {lng: -80.67915, lat: 40.18675}, {lng: -80.70884, lat: 40.11688}, {lng: -80.73918, lat: 40.07415}, {lng: -80.73634, lat: 40.03013}, {lng: -80.73958, lat: 39.97809}, {lng: -80.7895, lat: 39.91874}, {lng: -80.85086, lat: 39.74113}, {lng: -80.99645, lat: 39.56936}, {lng: -81.10734, lat: 39.4772}, {lng: -81.32789, lat: 39.35731}, {lng: -81.65907, lat: 39.27704}, {lng: -81.74372, lat: 39.14296}, {lng: -81.80564, lat: 39.08398}, {lng: -81.78105, lat: 39.03248}, {lng: -81.76608, lat: 38.923}, {lng: -81.98396, lat: 39.00578}, {lng: -82.08866, lat: 38.97618}, {lng: -82.13327, lat: 38.90618}, {lng: -82.20789, lat: 38.76425}, {lng: -82.19786, lat: 38.59253}, {lng: -82.30493, lat: 38.49394}, {lng: -82.52025, lat: 38.4078}, {lng: -82.5957, lat: 38.4217}, {lng: -82.59605, lat: 38.42232}, {lng: -82.63421, lat: 38.48317}, {lng: -82.69107, lat: 38.5372}, {lng: -82.81866, lat: 38.57197}, {lng: -82.85643, lat: 38.64709}, {lng: -82.87029, lat: 38.72288}, {lng: -82.92852, lat: 38.74861}, {lng: -83.03596, lat: 38.72015}, {lng: -83.16023, lat: 38.62004}, {lng: -83.18992, lat: 38.61781}, {lng: -83.25246, lat: 38.62524}, {lng: -83.32044, lat: 38.62148}, {lng: -83.38375, lat: 38.66266}, {lng: -83.5321, lat: 38.70224}, {lng: -83.60633, lat: 38.68627}, {lng: -83.63688, lat: 38.66866}, {lng: -83.64276, lat: 38.64348}, {lng: -83.68368, lat: 38.63132}, {lng: -83.76657, lat: 38.6537}, {lng: -83.85462, lat: 38.75335}, {lng: -83.99038, lat: 38.78368}, {lng: -84.21217, lat: 38.8054}, {lng: -84.29624, lat: 38.98255}, {lng: -84.49374, lat: 39.10246}, {lng: -84.58023, lat: 39.08038}, {lng: -84.81682, lat: 39.10582}, {lng: -84.82016, lat: 39.10548}, {lng: -84.82016, lat: 39.10555}, {lng: -84.81983, lat: 39.24848}, {lng: -84.81921, lat: 39.31752}, {lng: -84.81743, lat: 39.40532}, {lng: -84.81505, lat: 39.56565}, {lng: -84.8146, lat: 39.67734}, {lng: -84.81413, lat: 39.79934}, {lng: -84.81161, lat: 39.96958}, {lng: -84.80607, lat: 40.19421}, {lng: -84.8039, lat: 40.29178}, {lng: -84.80389, lat: 40.35235}, {lng: -84.80346, lat: 40.46558}, {lng: -84.80251, lat: 40.56906}, {lng: -84.80219, lat: 40.69148}, {lng: -84.8023, lat: 40.81594}, {lng: -84.80269, lat: 40.88703}, {lng: -84.8032, lat: 40.97564}, {lng: -84.80333, lat: 41.07024}, {lng: -84.80338, lat: 41.18506}, {lng: -84.80359, lat: 41.27179}, {lng: -84.80388, lat: 41.31001}, {lng: -84.80393, lat: 41.40873}, {lng: -84.80421, lat: 41.47287}, {lng: -84.80499, lat: 41.55718}, {lng: -84.8056, lat: 41.60389}, {lng: -84.80597, lat: 41.69464}, {lng: -84.80597, lat: 41.69612}, {lng: -84.79846, lat: 41.69638}, {lng: -84.70871, lat: 41.69912}, {lng: -84.67018, lat: 41.69997}, {lng: -84.56953, lat: 41.702}, {lng: -84.45746, lat: 41.7044}, {lng: -84.40308, lat: 41.70574}, {lng: -84.32847, lat: 41.70747}, {lng: -84.21543, lat: 41.71047}, {lng: -84.14889, lat: 41.71248}, {lng: -84.07437, lat: 41.71492}, {lng: -83.93801, lat: 41.71874}, {lng: -83.85652, lat: 41.72117}, {lng: -83.76451, lat: 41.7235}, {lng: -83.70947, lat: 41.72524}, {lng: -83.68255, lat: 41.72664}, {lng: -83.65588, lat: 41.72723}, {lng: -83.63154, lat: 41.72793}, {lng: -83.59474, lat: 41.72904}, {lng: -83.56863, lat: 41.72966}, {lng: -83.53156, lat: 41.73076}, {lng: -83.51951, lat: 41.73115}, {lng: -83.4983, lat: 41.73179}, {lng: -83.4883, lat: 41.73201}, {lng: -83.46885, lat: 41.73253}, {lng: -83.41585, lat: 41.73379}, {lng: -83.11246, lat: 41.95941}, {lng: -83.07365, lat: 41.87505}, {lng: -83.06931, lat: 41.86422}, {lng: -83.06694, lat: 41.86308}, {lng: -83.06652, lat: 41.86287}, {lng: -82.99992, lat: 41.83106}, {lng: -82.99557, lat: 41.82897}, {lng: -82.87491, lat: 41.77081}, {lng: -82.83164, lat: 41.75005}, {lng: -82.83174, lat: 41.75005}, {lng: -82.80656, lat: 41.73789}, {lng: -82.80612, lat: 41.73768}, {lng: -82.76387, lat: 41.71729}, {lng: -82.74991, lat: 41.71}, {lng: -82.72643, lat: 41.69831}, {lng: -82.68912, lat: 41.68021}, {lng: -82.68112, lat: 41.67633}, {lng: -82.65376, lat: 41.67643}, {lng: -82.6249, lat: 41.67654}, {lng: -82.49989, lat: 41.67669}, {lng: -82.4206, lat: 41.67656}, {lng: -82.39835, lat: 41.67652}, {lng: -82.37488, lat: 41.68735}, {lng: -82.34812, lat: 41.70006}, {lng: -82.33255, lat: 41.70745}, {lng: -82.24988, lat: 41.74619}, {lng: -82.24149, lat: 41.75003}, {lng: -82.12487, lat: 41.80494}, {lng: -81.99986, lat: 41.86379}, {lng: -81.97657, lat: 41.87503}, {lng: -81.97058, lat: 41.87785}, {lng: -81.87486, lat: 41.92276}, {lng: -81.78837, lat: 41.96332}, {lng: -81.74985, lat: 41.98137}, {lng: -81.7092, lat: 42.00004}, {lng: -81.68645, lat: 42.00004}, {lng: -81.66584, lat: 42.01031}, {lng: -81.62485, lat: 42.03073}, {lng: -81.62485, lat: 42.02962}, {lng: -81.55304, lat: 42.06656}, {lng: -81.49984, lat: 42.09392}, {lng: -81.49518, lat: 42.0964}, {lng: -81.45429, lat: 42.116}, {lng: -81.43542, lat: 42.12504}, {lng: -81.37484, lat: 42.15467}, {lng: -81.33208, lat: 42.17579}, {lng: -81.29143, lat: 42.19587}, {lng: -81.27243, lat: 42.20525}, {lng: -81.24983, lat: 42.21642}, {lng: -81.21192, lat: 42.22227}, {lng: -81.19098, lat: 42.2255}, {lng: -81.17417, lat: 42.22785}, {lng: -81.12376, lat: 42.23491}, {lng: -81.10695, lat: 42.23727}, {lng: -81.08754, lat: 42.24034}, {lng: -81.02931, lat: 42.24954}, {lng: -81.02614, lat: 42.25005}, {lng: -81.01944, lat: 42.25005}, {lng: -81.00983, lat: 42.2516}, {lng: -81.00557, lat: 42.25229}, {lng: -81.00445, lat: 42.25231}, {lng: -80.99982, lat: 42.2524}, {lng: -80.97907, lat: 42.2558}, {lng: -80.94371, lat: 42.26127}, {lng: -80.90318, lat: 42.26771}, {lng: -80.87482, lat: 42.27228}, {lng: -80.83553, lat: 42.27841}, {lng: -80.79198, lat: 42.28512}, {lng: -80.74982, lat: 42.2915}, {lng: -80.70754, lat: 42.29829}, {lng: -80.66873, lat: 42.30398}, {lng: -80.62481, lat: 42.31076}, {lng: -80.60477, lat: 42.31422}, {lng: -80.57532, lat: 42.31872}, {lng: -80.52967, lat: 42.32568}, {lng: -80.51938, lat: 41.95513}, {lng: -80.51935, lat: 41.88874}, {lng: -80.51938, lat: 41.82006}, {lng: -80.51932, lat: 41.75918}, {lng: -80.51941, lat: 41.71723}, {lng: -80.51941, lat: 41.67413}, {lng: -80.51936, lat: 41.64064}, {lng: -80.51935, lat: 41.55044}, {lng: -80.51923, lat: 41.51721}, {lng: -80.51911, lat: 41.44163}, {lng: -80.5191, lat: 41.34961}, {lng: -80.51908, lat: 41.29158}, {lng: -80.5191, lat: 41.24365}];
	
	// Construct the polygon.
    var ohioState = new google.maps.Polygon({
      paths: [world, ohioBounds],
      strokeColor: '#9D2235', //'#fbf7f4' whitish -- #9D2235 red
      strokeOpacity: 1,
      strokeWeight: 4,
      fillColor: '#2D2926', //'#9D2235',
      fillOpacity: 0.2
    });
    ohioState.setMap(map);

    if( pageCatID ) {
        markerFetch.catID = pageCatID;
        fetchMarkers(markerFetch);    
    } else {
        if(getUrlParameter('category')) {
            var softFilter = getUrlParameter('category');
            markerCat.push(softFilter);

            markerFetch.tagName = softFilter;
            fetchMarkers(markerFetch);
        } else {
            fetchMarkers(markerFetch);
        }
    }
    /* WHAT ARE THIS?
    i   f(fetch == 'ya') {
            markerCat.push(fetch.urlRoute);

            markerFetch.tagName = fetch.urlRoute;
            fetchMarkers(markerFetch);
            console.log(fetch.urlRoute);
        } else 

    */

    
    google.maps.event.addListener(map, "click", function(event) {
        iw.close();
        sessionStorage.removeItem('pagePortal');
		$('#infoWindow').removeClass('open', function(){
			$('#application').removeClass('info');
            $('#iw-header').css('background-image','');
            iconReset(markerTable);
		});
	});
	google.maps.event.addListener(map, "dragstart", function(event) {
        iw.close();
        sessionStorage.removeItem('pagePortal');
		$('#application').removeClass('info');
		$('#infoWindow').removeClass('open', function(){
			$('#iw-header').css('background-image','');
            iconReset(markerTable);
		});
    });
}