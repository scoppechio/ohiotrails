window.onload = function() {

    var canvas = document.getElementById('snow-canvas'),
        ctx = canvas.getContext('2d'),
        canvasWidth = (canvas.width = window.innerWidth),
        canvasHeight = (canvas.height = window.innerHeight),
        particles = {},
        particleIndex = 0,
        particleNum = 1,
        vy = 1.5,
        vx = Math.random(),
        mlife = 350;
    
    window.onresize = function(event) {
      canvasWidth = (canvas.width = window.innerWidth);
      canvasHeight = (canvas.height = window.innerHeight);
    }
  
    //ctx.fillStyle = 'rgba(0,0,0,.2)';
    //ctx.fillRect(0, 0, canvasWidth, canvasHeight);
  
    function Particle() {
      this.x = Math.floor(Math.random() * canvasWidth - 100);
      this.y = -20;
      this.vx = vx;
      this.vy = vy;
      this.size = Math.floor(Math.random() * 4) + 1;
      this.gravity = 0.02;
      if (this.size > 2) {
          this.vy += 0.2;
      }
      else if (this.size < 2) {
        this.vy -= 0.2;
      }
      particleIndex++;
      particles[particleIndex] = this;
      this.id = particleIndex;
      this.life = 0;
      this.maxLife = mlife;
  
  
    }
  
    Particle.prototype.draw = function(r, v) {
      r = Math.floor(Math.random() * 100);
      v = 50;
      this.x += this.vx;
      this.y += this.vy;
      if (r < v) {
          this.vx -= Math.random() * 1 - 0.5;
        this.vy += this.vy/1000;
      }
      this.vy += this.gravity;
      this.life++;
      if (this.life >= this.maxLife) {
        delete particles[this.id];
      }
  
      ctx.fillStyle = 'rgba(255, 255, 255, 0.5)';
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2, false);
      ctx.fill();
    };
  
  
    setInterval(function() {
      // ctx.fillStyle = 'rgba(0, 0, 0, 0)';
      ctx.clearRect(0, 0, canvasWidth, canvasHeight);
      for (var i = 0; i < particleNum; i++) {
        new Particle();
      }
  
      for (var i in particles) {
        particles[i].draw();
      }
    }, 15);
  }; 