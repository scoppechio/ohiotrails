Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

function setCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function unsetCookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

function photoLoad(photo){
    var $thisSlide = $('.slick-active').find('.iw-content-wrap');
    
	$thisSlide.find(".photo-bucket li:last-child").each(function (index) {
		$(this).css('background-image', 'url(' + photo + ')');
	});
}

function slideLoad(meta){
    var $thisSlide = $('.slick-active').find('.iw-content-wrap');
    
    $('.slick-active').find(".iw-header").css('background-image', 'url(' + meta.photos + ')');
	$thisSlide.find('.item-rating').attr('data-rating', meta.rating);
	$thisSlide.find('.gdRating').find('.star--top').width((meta.rating * 100) / 5 + '%');
    $thisSlide.find('.gdRating').show();

    if( meta.hours !== undefined ){
        for(i=0; i<meta.hours.length; i++){
            if(i == today){
                $thisSlide.find('.hours').append('<li title="Today" class="today">' + meta.hours[i] + '</li>');
            } else {
                $thisSlide.find('.hours').append('<li>' + meta.hours[i] + '</li>');
            }
        }
    }
	
	if(meta.www !== undefined){
	    $thisSlide.find('.website').append('<a href="' + meta.www + '" target="_blank">' + meta.www + '</a>');
	}
}

function callback(results, status) {
    var $thisSlide = $('.slick-active').find('.iw-content-wrap');
    var headerUrl = $("#infoWindow").data('page-thumb');
    
	if (status == google.maps.places.PlacesServiceStatus.OK) {
		var thisPlace = { placeId: results[0].place_id};

		places.getDetails(thisPlace, shop);
		function shop(item, status) {			    
  			if (status == google.maps.places.PlacesServiceStatus.OK) {
                if( typeof item.rating != 'undefined') { places_meta.rating = item.rating; }
                if( typeof item.opening_hours != 'undefined'){ places_meta.hours = item.opening_hours.weekday_text; }
  				if( typeof item.website != 'undefined'){ places_meta.www = item.website; }

  				if(item.photos && $thisSlide.find(".photo-bucket").length === 0){
  				    if(item.photos.length > 3) 
  				    {
                        $thisSlide.append('<ul class="photo-bucket" />');
                        places_meta.photos = item.photos[0].getUrl({'maxWidth': 350, 'maxHeight': 200});

                        slideLoad( places_meta );
                          
      				    for(i=1; i<7; i++){
      				        if(typeof item.photos[i] !== 'undefined')
      				        { 
      				            $thisSlide.find('.photo-bucket').append('<li/>');
          				        photoLoad( item.photos[i].getUrl({'maxWidth': 350, 'maxHeight': 600}) ); 
      				        } else {
      				            break;
      				        }
          				}
  				    } else {
  				        $thisSlide.prev('.iw-header').css('background-image', 'url(' + headerUrl + ')');
  				        $thisSlide.append('<ul class="photo-bucket"><li>No images to show</li></ul>');
  				    }
  				} else if($thisSlide.find(".photo-bucket").length !== 0) {
                    
  				} else {
  				    $thisSlide.prev('.iw-header').css('background-image', 'url(' + headerUrl + ')');
  				    $thisSlide.append('<ul class="photo-bucket"><li>No images to show</li></ul>');
  				}
  			}
		} 
	} else {
	    $thisSlide.prev('.iw-header').css('background-image', 'url(' +headerUrl + ')');
	    
        if($thisSlide.find(".photo-bucket").length === 0){
  		    $thisSlide.append('<ul class="photo-bucket"><li>No images to show</li></ul>');
        }
	}
}

function categoryFilter(markerCat, markerFetch, catSelect) {
    if(catSelect) {
        $.when(removeMarkerCat(markerCat, catSelect)).then(removeMarkers()).then(fetchMarkers(markerFetch)).then(fetchList(markerFetch));
    } else {
        $.when(removeMarkerCat()).then(removeMarkers()).then(fetchMarkers(markerFetch)).then(fetchList(markerFetch));
    }
}

function removeMarkerCat(catList) {
    var category, a = arguments, L = a.length, ax;
    while (L > 1 && catList.length) {
        category = a[--L];
        while ((ax= catList.indexOf(category)) !== -1) {
            catList.splice(ax, 1);
        }
    }
    return catList;
}

function iconReset(allMarkers){
    for (i=0; i < allMarkers.length; i++) 
    {
        allMarkers[i].setIcon({
            url: allMarkers[i].icon.url,
            scaledSize: new google.maps.Size(70, 70)
        });
        allMarkers[i].setZIndex(9);
    }
}

function onXMLLoadFailed(xhr){
    console.log("An Error has occurred.");
    console.log(xhr);
}

function loadStart(){ document.getElementById('page-load').classList.add('loading'); }
function loadFinish(){ 
    document.getElementById('page-load').classList.remove('loading');
    google.maps.event.addListenerOnce(map, 'idle', function(){
        google.maps.event.trigger(map, "resize");
   });
}

function removeMarkers(catArray){
    // loadStart();
    
    for(i=0; i<markerTable.length; i++){
        markerTable[i].setMap(null);
    }

    markerTable = [];
}
function initSlick(carousel) {
    var slickOptions = {
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:	$('.slick-prev'),
		nextArrow: $('.slick-next'),
		adaptiveHeight: true
    };

    setTimeout(function(){
        carousel.slick(slickOptions);
        loadFinish(); 

        for (i=0; i < markerTable.length; i++) {
            if(sessionStorage.pagePortal == markerTable[i].page) {
                google.maps.event.trigger(markerTable[i], 'click');
            }
        }
    }, 500); 
}

function getSlick(carousel, trailMarker, event) {
    var trailId = trailMarker.id;
    var trailIndex = $('.slick-track').find("[data-postid="+trailId+"]").closest('.slick-slide').data("slick-index");

    carousel.slick('slickGoTo', trailIndex).on('afterChange', function(event, slick, currentSlide){
        var onSlide = $(slick.$slides[currentSlide]);
        var slideId = onSlide.find('.iw-bounds').data('postid');
        for (i=0; i < markerTable.length; i++) {
            if(slideId == markerTable[i].id) {
                var thisMarker = markerTable[i];
                $.when(iconReset(markerTable)).then(function(){
                    thisMarker.setIcon({
                        url: thisMarker.icon.url,
                        scaledSize: new google.maps.Size(120, 120),
                        size: new google.maps.Size(120, 120)
                    });
                    thisMarker.setZIndex(99);
                    map.panTo(thisMarker.getPosition());
                    if( w < 600)
			        {
                        // map.panBy(0,100);
			        }

                    // Get Google places if Trail Item
                    if( !is_root && !$('#application').data('birding') ){   
                        var request = {
                            keyword: thisMarker.title,
                            location: thisMarker.position,
                            radius: 500
                        };
                        places = new google.maps.places.PlacesService(map);
                        places.nearbySearch(request, callback);
                    }
                });
                //google.maps.event.trigger(markerTable[i], 'click');
            }
        }

        dataLayer.push({'event': 'slide-select'});           
        
    }).on('beforeChange', function(event, slick, currentSlide){
        
    });

}

function markerSelect(allTheMarkers){
    for (i=0; i < allTheMarkers.length; i++) {

    }
}

function makeIcons(category, templateUrl, scale){
    var ico;

    var spirits = templateUrl + '/img/icons/pin-spirits.svg';
    var coffee = templateUrl + '/img/icons/pin-coffee.svg';
    var food = templateUrl + '/img/icons/pin-food.svg';
    var shopping = templateUrl + '/img/icons/pin-shopping.svg';
    var sightseeing = templateUrl + '/img/icons/pin-sightseeing.svg';
    var artmuesum = templateUrl + '/img/icons/pin-artmuseum.svg';
    var donut = templateUrl + '/img/icons/pin-donut.svg';
    var iceCream = templateUrl + '/img/icons/pin-ice-cream.svg';
    
    if(scale == 1) { var size = new google.maps.Size(120, 120); }
    else { var size = new google.maps.Size(70, 70); }
    
    if( category.includes('spirits') ){
        ico = {
			url: spirits,
			scaledSize: size
		}
    } else if( category.includes('coffee') ){
        ico = {
			url: coffee,
			scaledSize: size
		}
    } else if( category.includes('ice-cream') ){
        ico = {
			url: iceCream,
			scaledSize: size
        }
    } else if( category.includes('food') ){
        ico = {
			url: food,
			scaledSize: size
		}
    } else if( category.includes('shopping') ){
        ico = {
			url: shopping,
			scaledSize: size
		}
    } else if( category.includes('sightseeing') ){
        ico = {
			url: sightseeing,
			scaledSize: size
		}
    } else if( category.includes('artmuseum') ){
        ico = {
			url: artmuesum,
			scaledSize: size
		}
    } else if( category.includes('donut') ){
        ico = {
			url: donut,
			scaledSize: size
		}
    }

    return ico;
}

function makeMarkers(map, markers){
    if(markers.icon)
    {
        markIco = {
            url: markers.icon,
            scaledSize: new google.maps.Size(70, 70)
        };
    } else {
        markIco = makeIcons(markers.category, templateURL, 0);
    }

    marker = new google.maps.Marker({
        map: map,
        title: markers.title,
        page: markers.page,
        position: markers.loc,
        id: markers.id,
        icon: markIco,
        category: markers.category,
        animation: google.maps.Animation.DROP
    }); 

    

    // Marker Click Event Handler
    marker.addListener('click', function(){
        $('.pill-holder').removeClass('filter open');
        $('#featureWindow, #sidebar').removeClass('open');

        // scale up this icon
        var thisMarker = this;
        
        $.when(iconReset(markerTable)).then(function(){
            thisMarker.setIcon({
                url: thisMarker.icon.url,
                scaledSize: new google.maps.Size(120, 120),
                size: new google.maps.Size(120, 120)
            });
            thisMarker.setZIndex(99);
        });

        if(siteURL){
            if( w < 600)
            {
                map.setZoom(12);
                // map.panTo(thisMarker.getPosition());
            } else {
                if(map.getZoom() < 9){ map.setZoom(9); }
                // map.panTo(thisMarker.getPosition());
            }
               
        } else {
            if( w < 600)
            {
                map.setZoom(12);
                // map.panTo(thisMarker.getPosition());
                //map.panBy(0,100);
            } else {
                if(map.getZoom() < 12){ map.setZoom(12); }
                // map.panTo(thisMarker.getPosition());
            }
        }
        
        // InfoWindow Formatting
		$('#application').addClass('info');
		$('#page-content').addClass('close');
		
		if(!$('#infoWindow').hasClass('open')){
			$('#infoWindow').addClass('open');
        }
        
        // Get Google places if Trail Item
		if( !is_root && !$('#application').data('birding') && !$('body').hasClass('page-template-category-nav') ){   
            var request = {
                keyword: thisMarker.title,
                location: thisMarker.position,
                radius: 500
            };
            places = new google.maps.places.PlacesService(map);
            places.nearbySearch(request, callback);
        }

        // Navigate InfoWindow Carousel to selected marker
        getSlick($('#carousel'), thisMarker, 'click');
    });

    google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
            iw.setContent(marker.title);
            iw.open(map, marker);
        }
    })(marker, i));

    google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
        return function() {
            iw.close();
        }
    })(marker, i));

    markerTable.push(marker);
    
    return marker;
}

function markerLoop(markerList){
    iw = new google.maps.InfoWindow();

    for (i=0; i < markerList.length; i++) 
    {
        markerCatch.category = markerList[i].getAttribute('category');
        markerCatch.id = markerList[i].getAttribute('id');
        markerCatch.title = markerList[i].getAttribute('title');
        markerCatch.page = markerList[i].getAttribute('page');
        markerCatch.address = markerList[i].getAttribute('address');    
        markerCatch.icon = markerList[i].getAttribute('iconUrl');
        markerCatch.loc = new Object({
            lat: parseFloat(markerList[i].getAttribute('lat')),
            lng: parseFloat(markerList[i].getAttribute('lng'))
        });

        makeMarkers( map, markerCatch );

        bounds.extend(markerCatch.loc);
    } // Marker Loop
}

function loadCarousel(postLoop) {
    var $slickBox = $('#carousel');   
    var params = { category_id: postLoop.catID };    

    if(postLoop.bird){ params.birding = postLoop.bird; }
    if(postLoop.category){ params.category = postLoop.category; }
        
    $.ajax({
		url: templateURL + '/postLoop.php',
		data: params,
		type: "GET",
		dataType: "html",
		beforeSend:function(xhr){
            loadStart();

			$("#infoWindow").removeClass('open', function(){
			    $('#iw-header').css('background-image','');
			    
			    if( $slickBox.children().length > 0 ) { $slickBox.empty().slick('unslick'); }
            });
		},
		success:function(data){
			$.when($slickBox.html(data)).then(function(){
			    var headHeight = $('header').outerHeight();
			    var breadcrumbHeight = $('#trail-home').outerHeight();
                $('#infoWindow').css({ 'margin-top': (headHeight + breadcrumbHeight) });
                $('.iw-stop-wrap').accordion({
                    collapsible: true,
                    active: false,
                    heightStyle: "content"
                });
            }); // insert data
		},
		complete:function(){
            initSlick($slickBox);
		}
	});
}

function fetchMarkers(options){
    var filename = templateURL + '/markerLoad.php';
    if(options.catID){ var params = { category_id: options.catID }; }
    if(options.tagName){ var params = $.param({ category: options.tagName }, true); }

    goGetIt = $.ajax({
        type: "GET",
        data: params,
        url: filename ,
        dataType: "xml",
        beforeSend: function(jqXHR, settings){

        },
        success: function(data){
            var markers = data.documentElement.getElementsByTagName("marker");
            markerLoop(markers);
        },
        complete:function(){   
            map.fitBounds(bounds); 
            map.panToBounds(bounds);            
            
            if(params){
                if(is_root){
                    postLoop.category = options.tagName;
                    loadCarousel(postLoop);
                    fetchList(markerFetch);

                    $('.categoryFilter li').each(function(){
                        if($(this).find('button').data('category') === postLoop.category){
                            $(this).addClass('active');
                        }
                    });
                } else {
                    postLoop.category = $('#application').data('post-tag');
                    postLoop.tagName = $('#application').data('post-tag');
				    postLoop.catID = $('#application').data('catid');
                    postLoop.bird = $('#application').data('birding');
				
                    if(postLoop.bird){
                        loadCarousel(postLoop);
                        fetchList(markerFetch);
                    } else {
                        loadCarousel(postLoop);
                        fetchList(markerFetch);
                    }
                }
            } else {
                loadCarousel(postLoop);
                fetchList(markerFetch);
            }
        },
        error : onXMLLoadFailed
    });
}

function fetchList(options){
    var filename = templateURL + '/listLoad.php';
    if(options.tagName) { var params = $.param({ tag: options.tagName }, true); }

    $.ajax({
        url: filename,
        data: params,
        type: "GET",
        dataType: "html",
        beforeSend:function(xhr, settings){
            //loadStart();
        },
        success:function(data){ 
            $('#list-bucket').html(data);

        },
        complete:function(){
            // loadFinish();

            $('.post').each(function(i){
                var $postPanel = $(this);
                var $bgPanel = $postPanel.find('.lazy');
                var imgUrl = $bgPanel.data('img');
        
                $bgPanel.css({ "background-image": "url('"+imgUrl+"')" });
              
                setTimeout(function(){
                    $postPanel.addClass('lift');
                },50*i);
            });
        
            $('.post-link').on('click', 'button', function(){
                var markerId = $(this).data('post-id');
        
                $('.pill-holder').removeClass('list open');
                
                $('#post-wrap').removeClass('on', function(){
                    
                    $('.markerFilter').toggle("slide");
                    $('#ui-controls, body').removeClass('list-open');
                    for (i=0; i < markerTable.length; i++) {
                        if(markerId == markerTable[i].id) {
                            google.maps.event.trigger(markerTable[i], 'click');
                        }
                    }
                });
            });
        }
    });
}

 /************************************************

****************************************************/
