var wWidth = $(window).width();
$(document).ready(function(){
	
	if(wWidth >= 640){
		if($('#featureWindow').hasClass('open fall')){
			rainLeaves();
		}
	}
	
	$('#mapExplore').on('click', function(){
	    dataLayer.push({'event': 'splash-open'});
		$('#page').addClass('gingerbread').promise().done(function(){
			google.maps.event.addListenerOnce(map, 'idle', function(){
				google.maps.event.trigger(map, "resize");
				map.fitBounds(bounds); 
	    		map.panToBounds(bounds);
			});
		});
		$('#splash').addClass('exploring', function(){
			setCookie('visit_',true,7);
			setTimeout(function() {
        		$('#splash').hide();
    		}, 375);
		});	
	});

	$('#noSplash').on('click', function(){
		$('#splash').addClass('exploring', function(){
			// blob();
			setCookie('visit_',true,7);
			setTimeout(function() {
				$('#splash').remove();
				$('#new-kid').addClass('pop', function(){
					var imgContain = $(this);
					var tutorial = $(this).data('img');
					$.when(imgContain.find('.img-wrap').append('<img src="' + tutorial + '" />')).then(function(){
						imgContain.find('.img-wrap').find('img').addClass('bounce');
					});
				});
				fetchList(markerFetch);
			}, 375);
		});	
	});

	$('#new-kid').on('click', function(){
		$(this).removeClass('pop', function(){
			$('#new-kid').remove();
		});
	});
	
	$('#delCookie').on('click', function(){
		unsetCookie('visit_');
	});
	
	$('#nav-toggle').on('click',function(){
		$('#header_nav').slideToggle('fast').toggleClass('nav-open');
		$('#primary').toggleClass('nav-open');
	});
	
	$('#primary').on('click',function(){
		if($('#header_nav').hasClass('nav-open')){
			$('#primary, #header_nav').removeClass('nav-open');
			$('#header_nav').slideUp();
		}
	});

	$('#map-toggle').on('click', function(){		
		$('#post-wrap').addClass('on');
		$('#infoWindow').removeClass('open');
		$('#application').removeClass('info');
		$('#ui-controls, body').addClass('list-open');

		// $('#sidebar').addClass("open");
		$('.pill-holder').addClass('list open');
	});
	
	$('#filter-toggle').on('click', function(){
		// $('.markerFilter').toggle("slide").toggleClass('open');
		// $(this).find('svg').toggleClass('fa-ellipsis-v fa-caret-left');
		$('#sidebar').addClass('open');
		$('.pill-holder').addClass('filter open');
	});

	$('#close-side').on('click', function(){
		$('#sidebar').removeClass('open');
		$('.pill-holder').removeClass('filter list open');
		$('#ui-controls, body').removeClass('list-open');
		$('#post-wrap').removeClass('on');
	});
	
	$('#closeTrailInfo').on('click', function(){
		$(this).closest('#page-content').addClass('close');
	});
	$('#trail-toggle').on('click', function(){
		$('#page-content').removeClass('close');
		$('#infoWindow').removeClass('open');
	});
	
	$('.slick-arrow').on('click', function(){
		var arrowId = $(this).attr('id');
		dataLayer.push({'event': 'details-'+ arrowId});
	});
    
    $('.categoryFilter li button').each(function(){
        $(this).on('click', function(e){
			var categorySelect = $(this).data('category');
			var filterContainer = $(this).closest('div').attr('id');
			var btnId = $(this).attr('id');		
			var catClass = $(this).attr('class');

			sessionStorage.removeItem('pagePortal');
			iw.close();
			goGetIt.abort();

			if( $(this).closest('li').hasClass('active') ) {

				// Remove the filter selection
				$('.' + catClass).closest('li').removeClass('active');
				
				if( !$('.categoryFilter li').hasClass('active') ){
					markerCat.remove(categorySelect);
					
					// if no filter selected
					markerFetch = {};
					categoryFilter(markerCat, markerFetch);

					$('#featureWindow').removeClass('open');
					$('#application').removeClass('info');

					if(wWidth < 600) {
						$('#mapCanvas').css('height', 'calc(100vh - 60px)');
					}

				} else {
					markerCat.remove(categorySelect);
					// if AT LEAST one filter is still active
					categoryFilter(markerCat, markerFetch, categorySelect);

					if( !$('.filter-feature').hasClass('active') ){
						$('#featureWindow').removeClass('open');
						$('#application').removeClass('info');

						if(wWidth < 600) {
							$('#mapCanvas').css('height', 'calc(100vh - 60px)');
						}
					}
				}
			} else {

				// Add the filter selection
				$('.' + catClass).closest('li').addClass('active');

				// Make a list of selected Marker Categories
				markerCat.push(categorySelect);

				// Selecting this category as active
				markerFetch.tagName = markerCat;
				categoryFilter(markerCat, markerFetch);

				if(categorySelect === 'feature' && !$('#post-wrap').hasClass('on')){
					$('#featureWindow').addClass('open');
					$('#application').addClass('info');
				}
			}		
			
			dataLayer.push({'event': btnId + '-click'});
			dataLayer.push({'event': 'infoWindow-close'});
        });
        $(this).hover(function(){
            if(!$(this).closest('li').hasClass('active')){
                $(this).closest('li').addClass('hover');
            }
        }, function(){
            $(this).closest('li').removeClass('hover');
        });
	});
	
	if(!is_root) {
		$('#trail-home a').on('click', function(){
			var cameFrom = $('#application').data('post-tag');
			sessionStorage.setItem("pagePortal", cameFrom);
		});

		if (window.history && history.pushState) {
			addEventListener('load', function() {
				// history.pushState(null, null, null); // creates new history entry with same URL
				var cameFrom = $('#application').data('post-tag');
				sessionStorage.setItem("pagePortal", cameFrom);
				addEventListener('popstate', function() {
					var cameFrom = $('#application').data('post-tag');
					sessionStorage.setItem("pagePortal", cameFrom);
				});    
			});
		}
	}
	
	$('#view-feature').on('click', function(){
		google.maps.event.trigger(markerTable[0], 'click');
	});

	$('#ohio-logo').on('click', function(){
		sessionStorage.removeItem('pagePortal');
	});

	$('#iwClose').on('click', function(){
		$('#infoWindow').removeClass('open');
		$('#application').removeClass('info');
		dataLayer.push({'event': 'infoWindow-close'});
		map.fitBounds(bounds); 
		map.panToBounds(bounds);

		iconReset(markerTable);
	});
	
	$('#fwClose').on('click', function(){
		$('#featureWindow').removeClass('open');
		$('#application').removeClass('info');

		dataLayer.push({'event': 'featureWindow-close'});

		map.fitBounds(bounds); 
		map.panToBounds(bounds);

		iconReset(markerTable);
	});

	if(wWidth < 600) {
		if($('#featureWindow').hasClass('open')){
			$('#mapCanvas').css('height', '40vh');
		}
	}
});