var falling;
var total = 30;
var roots = document.getElementsByClassName("fall");

function rakeLeaves(){
	var treeContainer = document.getElementById("leaves");
	var leaves = document.getElementsByClassName("leaf");
	
	while(leaves.length > 0){
        treeContainer.removeChild(leaves[0]);
  }
	roots.removeChild(treeContainer);

	falling = false;
	
}

var rainLeaves = (function() {
	var tree = document.createElement("div");
	tree.setAttribute("id", "leaves");
	
	falling = false;
	
	return function(){
		if(!falling){
			roots.prepend(tree);
		  var treeContainer = document.getElementById("leaves");
		
		  TweenLite.set("#leaves",{perspective:600});
		
		  var container = treeContainer,	w = window.innerWidth , h = (window.innerHeight * 2);
		  
		  for (i=0; i<total; i++){ 
		  	(function(){
		  		var Div = document.createElement('div');
		    	TweenLite.set(Div,{attr:{class:'leaf leaf--one'},x:R(0,w),y:R(-200,-150),z:R(-200,200)});
		    	treeContainer.appendChild(Div);
		    	animm(Div);
		  	})();
		  }

		  function animm(elm){   
		    TweenMax.to(elm,R(6,15),{y:h+100,ease:Linear.easeNone,repeat:-1,delay:-15});
		    TweenMax.to(elm,R(4,8),{x:'+=100',rotationZ:R(0,180),repeat:-1,yoyo:true,ease:Sine.easeInOut});
		    TweenMax.to(elm,R(2,8),{rotationX:R(0,360),rotationY:R(0,360),repeat:-1,yoyo:true,ease:Sine.easeInOut,delay:-5});
		  };
		
		  function R(min,max) {return min+Math.random()*(max-min)};
		  
		  falling = true;
		} 
	}
})();

var wWidth = $(window).width();

$(document).ready(function(){	
	$('#track').fullpage({
		licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
		scrollOverflow: true,
		dragAndMove: true,
		onLeave: function(origin, destination, direction){
			if(destination.isLast) {
				$('#controls-header').removeClass('on');
			} else {
				
			}

			destination.index > 1 ? $('#snow-canvas').hide() : $('#snow-canvas').show();
		},
		afterLoad: function(origin, destination, direction){
			if(roots.length > 0){
				if(destination.index <= 0 && wWidth >= 640){
						rainLeaves();
						$('#leaves').fadeIn();
				} else {
					$('#leaves').fadeOut(function(){ rakeLeaves() });
				}
			}
			
			destination.index > 1 && destination.isLast !== true ? $('#controls-header').addClass('on') : $('#controls-header').removeClass('on');
						
			if(destination.isLast) {
				$('#scroll-wrap').hide();
				$('.start-exploring-bg').addClass('done');
			} else {
				$('#scroll-wrap').show();
				$('.start-exploring-bg').removeClass('done');
			}
		}
	});
	
	$('.sq-thumb').each(function(){
		var feaThumb = $(this).data('thumb');
		$(this).css('background-image', 'url(' + feaThumb + ')');
	});
	$('.bg-blur').each(function(){
		var feaThumb = $(this).data('bgimg');
		$(this).css('background-image', 'url(' + feaThumb + ')');
	});
});