<?php
    // load wordpress into template
    define('WP_USE_THEMES', false);
    require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

	global $post;
	
	$category_name = $_GET['category'];

	if( isset($_GET['category_id']) )
	{
		$category_id = $_GET['category_id'];
        
        $args = array(
			'post_type'=> 'trail-stops',
            'category__in' => $category_id,
		    'numberposts' => -1,
		    'orderby' => 'title',
		    'order' => 'ASC'
        );
        
        $headTitle = get_cat_name($category_id);
        $page = get_page_by_title($headTitle);
	} else {
		if($category_name === 'feature'){
			$args = array(
				'numberposts' => -1,
				'post_type'=> 'trail-head',
				'tag_slug__in' => $category_name,
				'meta_key' => 'carousel_order',
				'orderby' => 'meta_value title',
				'order' => 'ASC'
			);
		} elseif($category_name !== 'feature'){
			$args = array(
				'numberposts' => -1,
				'post_type'=> 'trail-head',
				'tag_slug__in' => $category_name,
				'orderby' => 'title',
				'order' => 'ASC'
			);
		} else {
			$args = array(
				'numberposts' => -1,
				'post_type'=> 'trail-head',
				'orderby' => ' title',
				'order' => 'ASC'
			);
		}
        
	}
    
    $the_query = get_posts($args);
    
    foreach ($the_query as $item) { 
        
    $catTrail = array(
		'post_type'=> 'trail-stops',
    	'category_name' => $item->post_name,
    	'posts_per_page'=> -1
    );
	$findMatch = get_posts($catTrail);
?>
    <div class="iw-bounds trail-head" data-postid="<?php echo $item->ID; ?>">
        <span class="iw-header"><img src="<?php echo get_the_post_thumbnail_url($item->ID); ?>" /></span>
		
		<div class="iw-content-wrap <?php if( isset($_GET['category_id']) ) { echo 'trail'; } ?>">
		<?php if( get_field('google_places_list_url', $item->ID) && isset($_GET['category_id']) && !isset($_GET['birding']) ){ ?>
			<p class="go-explore">
				<a id="export-trail-<?php echo $item->ID; ?>" class="detailExport radial radial--org radial--long" target="_blank" href="<?php echo get_field('google_places_list_url', $item->ID, false); ?>">
					<?php echo get_template_part('img/icons/inline','backpack_icon.svg'); ?>
					<span>Export Trail</span>
				</a>
			</p>
		<?php } ?>
		<?php if( isset($_GET['category_id']) && !isset($_GET['birding']) ) { ?>
			<h2 class="trail-title">- <?php echo $headTitle; ?> -</h2>
		<?php } ?>
			
			<h2 class="item-title"><?php echo $item->post_title; ?></h2>
			
		<?php if( isset($_GET['category_id']) && !isset($_GET['birding']) ) { // inside Trail Item ?>
			<p class="item-address">
				<a id="trail<?php echo $item->ID; ?>-address" class="trail-directions" href="https://www.google.com/maps/search/?api=1&query=<?php echo get_geocode_address( $item->ID ); ?>" target="_blank"><?php echo get_geocode_address( $item->ID ); ?></a>
			</p>
			<span class="item-rating">
				<ul class="gdRating">
					<li class="star star--top"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></li>
					<li class="star star--bot"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></li>
				</ul>
			</span>
			<?php 
				if( $category_name == 'holiday-lights-trail' ){  
					echo '<p class="timeline">' . $item->post_content . '</p>';
				} else {
			?>
			<ul class="hours"></ul>
			<?php } ?>
			<p class="website"></p>
		<?php 
			} else { 
		?>
			<h3 class="trail-count">
				<span><?php if(count($findMatch) == 1) { echo '1 Stop'; } else { echo count($findMatch).' Stops'; } ?></span> <?php if(get_field('trail_website', $item->ID)){ echo '<a id="trail'.$item->ID.'-webUrl" class="websiteUrl" href="'.get_field('trail_website', $item->ID).'" target="_blank"><span>Website</span> <i class="fas fa-external-link-alt"></i></a>'; } ?>
			</h3>
		<?php if(count($findMatch) > 0) { ?>
			<?php if(get_field('google_places_list_url', $item->ID)){ ?>
    			<p class="go-explore">
					<a id="export-trail-<?php echo $item->ID; ?>" class="detailExport radial radial--org radial--long" target="_blank" href="<?php echo get_field('google_places_list_url', $item->ID, false); ?>">
    				<?php echo get_template_part('img/icons/inline','backpack_icon.svg'); ?>
    					<span>Export Trail</span>
    				</a>
    			</p>
			<?php } ?>
			<p class="go-trail">
				<?php if($item->post_name == 'cache-4-coins-geotour') { ?>
					<a id="trail-<?php echo $item->ID; ?>-stops" class="detailViewStops radial radial--blu radial--long" id="trail-link" href="<?php get_field('trail_website', $item->ID); ?>">Start Here</a> 
				<?php } else { ?>
					<a id="trail-<?php echo $item->ID; ?>-stops" class="detailViewStops radial radial--blu radial--long" id="trail-link" href="<?php echo get_permalink($item->ID); ?>">View Stops</a>
				<?php } ?>
			</p>
		<?php } ?>
			<div class="iw-content"><?php echo wpautop( $item->post_content ); ?></div>
			<div class="iw-stop-wrap">
				<h3>
					<span>See stops on this trail</span>
				</h3>
				<div>
					<ul class="iw-stops-list">
					<?php foreach( $findMatch as $trailStop) { ?>
						<li><?php echo $trailStop->post_title; ?></li>
					<?php } ?>
					</ul>	
				</div>
			</div>		
		<?php } ?>
		</div>
	</div>
<?php
    }
?>