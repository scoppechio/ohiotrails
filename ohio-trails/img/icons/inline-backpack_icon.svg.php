<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 30" style="enable-background:new 0 0 32 30;" xml:space="preserve">
<g>
	<path d="M21.3,24.3H10.7c-0.4,0-0.6,0.2-0.6,0.6v2.5c0,0.4,0.2,0.6,0.6,0.6h10.7c0.4,0,0.6-0.2,0.6-0.6v-2.5
		C21.9,24.5,21.7,24.3,21.3,24.3z M20.7,26.8h-9.5v-1.2h9.5V26.8z"/>
	<path d="M29.3,21.2h-1L27.4,9.6c-0.3-2.8-2.3-5.1-4.9-5.7c-0.5-0.1-1.1-0.2-1.7-0.2c-0.3-2.1-2-3.6-4.2-3.6h-1.2
		c-2.2,0-3.9,1.6-4.2,3.6c-3.3,0.1-6,2.5-6.4,5.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1L3.7,21.2h-1c-1.5,0-2.7,1.1-2.7,2.4v2.1
		c0,2.4,2.1,4.3,4.8,4.3H7c0.2,0,0.4-0.1,0.5-0.2c0.5,0.1,1,0.2,1.5,0.2h14c0.4,0,0.9-0.1,1.3-0.2c0.1,0.1,0.3,0.2,0.5,0.2h2.3
		c2.7,0,4.8-1.9,4.8-4.3v-2.1C32,22.2,30.8,21.2,29.3,21.2z M15.4,1.3h1.2c1.4,0,2.7,1,3,2.4h-7.1C12.7,2.3,13.9,1.3,15.4,1.3z
		 M6.3,8.4c0.8-2,2.7-3.5,5.1-3.5h0.4h8.5h0.6c0.8,0,1.6,0.2,2.3,0.5c1.4,0.7,2.5,2,2.9,3.6c-0.9,2.4-2.6,4.4-4.8,5.6V9.8h0.5
		c0.3,0,0.5-0.3,0.5-0.7S22,8.3,21.7,8.3h-1.9c-0.3,0-0.5,0.3-0.5,0.7s0.2,0.7,0.5,0.7h0.5v5.4c-1.3,0.6-2.8,0.9-4.3,0.9
		c-1.5,0-2.9-0.3-4.2-0.8V9.8h0.5c0.3,0,0.5-0.3,0.5-0.7s-0.2-0.7-0.5-0.7h-1.9C10,8.3,9.8,8.6,9.8,9s0.2,0.7,0.5,0.7h0.5v5
		C8.7,13.7,7,11.8,6,9.5C6.1,9.1,6.2,8.7,6.3,8.4z M4.8,28.7c-1.8,0-3.4-1.3-3.4-3v-2.1c0-0.6,0.6-1.1,1.3-1.1h0.9h0.1l-0.1,1.6
		c0,0.4,0,0.8,0,1.2c0.2,1.4,0.8,2.7,1.9,3.4H4.8z M26.2,27.5c-0.8,0.8-1.8,1.2-3,1.2h-14c-1.3,0-2.5-0.5-3.3-1.5
		c-0.3-0.3-0.5-0.6-0.7-0.9c-0.4-0.8-0.6-1.6-0.5-2.5l0.2-2.5l0.9-9.7c1.2,2,2.9,3.5,5,4.5v3.1h-0.5l-0.5,0.5v1.9l0.5,0.5h1.9
		l0.5-0.5v-1.9l-0.5-0.5h-0.5v-2.7c1.3,0.5,2.7,0.8,4.2,0.8c1.5,0,3-0.3,4.3-0.8v2.8h-0.5l-0.5,0.5v1.9l0.5,0.5h1.9l0.5-0.5v-1.9
		l-0.5-0.5h-0.5v-3.2c2.1-1.1,3.9-2.7,5.1-4.9l0.8,10.1l0.2,2.7C27.5,25.4,27.1,26.6,26.2,27.5C26.2,27.5,26.2,27.5,26.2,27.5z
		 M11.7,20.2v1h-1v-1H11.7z M21.2,20.2v1h-1v-1H21.2z M30.6,25.7c0,1.7-1.5,3-3.4,3h-0.6h-0.1c1.3-1,2-2.8,1.9-4.6l-0.1-1.6h0.1h0.9
		c0.7,0,1.3,0.5,1.3,1.1V25.7z"/>
</g>
</svg>
