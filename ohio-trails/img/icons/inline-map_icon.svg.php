<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">

<path d="M76.2,7.6l-2.5-1.2l-2.5,1.2L50,18.2L28.8,7.6l-2.5-1.2l-2.5,1.2L0,19.5v74.1l26.3-13.1L47.4,91l2.6,1.3
	l2.6-1.3l21.1-10.5L100,93.6V19.5L76.2,7.6z M23.6,76L5.2,85.1V22.8l18.3-9.2V76z M47.4,85.1L29.1,76V13.6l18.3,9.2V85.1z M70.9,76
	l-18.3,9.2V22.8l18.3-9.2V76z M94.8,85.1L76.4,76V13.6l18.3,9.2V85.1z"/>
</svg>
